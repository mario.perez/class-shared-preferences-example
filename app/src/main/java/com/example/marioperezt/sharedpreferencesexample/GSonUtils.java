package com.example.marioperezt.sharedpreferencesexample;

import com.google.gson.Gson;

/**
 * Created by marioperezt on 10/31/17.
 */

public class GSonUtils<T> {

    public String serializeToJson(T myClass) {
        Gson gson = new Gson();
        String j = gson.toJson(myClass);
        return j;
    }
    public T deserializeFromJson(String jsonString, Class<T> tClass) {
        Gson gson = new Gson();
        T myClass = gson.fromJson(jsonString, tClass);
        return myClass;
    }
}