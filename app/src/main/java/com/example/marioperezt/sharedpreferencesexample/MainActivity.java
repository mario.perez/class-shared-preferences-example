package com.example.marioperezt.sharedpreferencesexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView mTvValue;
    private TextView mTvClassSaved;
    private EditText mEtValue;
    private TextView mTvClassSavedList;
    private Button btSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferencesWrapper.getInstance().initialize(this);
        mTvValue = (TextView) findViewById(R.id.tv_value);
        mTvClassSaved = (TextView) findViewById(R.id.tv_result);
        mTvClassSavedList = (TextView) findViewById(R.id.tv_result_list);
        mEtValue = (EditText) findViewById(R.id.et_value);

        btSave = (Button) findViewById(R.id.bt_save);



        mTvValue.setText(SharedPreferencesWrapper.getInstance().getValue());
        mTvClassSaved.setText(SharedPreferencesWrapper.getInstance().getClassSaved());
        mTvClassSavedList.setText(SharedPreferencesWrapper.getInstance().getValueList());
        btSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        SharedPreferencesWrapper.getInstance().setValue(mEtValue.getText().toString());
        SharedPreferencesWrapper.getInstance().setClass(
                new SerializableDataModel(1,"Testing",100));
        Toast.makeText(this, "Saved",
                Toast.LENGTH_LONG).show();


        List<SerializableDataModel> result = new ArrayList<>();
        result.add(new SerializableDataModel(1,"Numero 1",100));
        result.add(new SerializableDataModel(2,"Numero 2",200));
        result.add(new SerializableDataModel(3,"Numero 3",300));
        result.add(new SerializableDataModel(4,"Numero 4",400));
        SharedPreferencesWrapper.getInstance().setClassList(result);


    }
}
