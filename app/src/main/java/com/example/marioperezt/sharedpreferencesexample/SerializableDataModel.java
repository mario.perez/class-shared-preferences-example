package com.example.marioperezt.sharedpreferencesexample;

import com.google.gson.annotations.SerializedName;

/**
 * Created by marioperezt on 10/31/17.
 */

public class SerializableDataModel {


    @SerializedName("id")
    private int mId;

    @SerializedName("Name")
    private String mName;

    @SerializedName("Grade")
    private int mGrade;

    public SerializableDataModel(int id, String name, int grade){
        mId = id;
        mName = name;
        mGrade = grade;
    }
    public SerializableDataModel(){

    }


    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getmGrade() {
        return mGrade;
    }

    public void setmGrade(int mGrade) {
        this.mGrade = mGrade;
    }
}
