package com.example.marioperezt.sharedpreferencesexample;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.List;

/**
 * Created by marioperezt on 10/31/17.
 */

public class SharedPreferencesWrapper {

    private SharedPreferencesWrapper(){

    }

    private class Keys{
        public static final String VALUE = "Value";
        public static final String VALUE_LIST = "ValueList";
        public static final String CLASS = "Class";
    }

    private static SharedPreferencesWrapper mInstance;
    private SharedPreferences mSharedPreferences;
    private Context mContext;

    public static SharedPreferencesWrapper getInstance(){
        if(mInstance == null)
            mInstance = new SharedPreferencesWrapper();

        return mInstance;
    }


    public void initialize(Context ctxt) {
        mContext = ctxt;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
    }



    public void setValue(String value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Keys.VALUE, value);
        editor.commit();
    }

    public String getValue() {
        return  mSharedPreferences.getString(Keys.VALUE, "");
    }


    public void setClassList(List<SerializableDataModel> value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Keys.CLASS,
                new GSonUtils<List<SerializableDataModel>>().serializeToJson(value));
        editor.commit();
    }

    public String getValueList() {
        return  mSharedPreferences.getString(Keys.VALUE_LIST, "");
    }

    public void setClass(SerializableDataModel value) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(Keys.CLASS,
                new GSonUtils<SerializableDataModel>().serializeToJson(value));
        editor.commit();
    }

    public String getClassSaved() {
        String result = mSharedPreferences.getString(Keys.CLASS, "");
        return result;
    }

}
















